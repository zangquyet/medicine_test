<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Repositories\MedicineRepository;
use Illuminate\Contracts\Routing\ResponseFactory;

class MedicinesController extends BaseController
{

    public function __construct(Request $request, MedicineRepository $repository, ResponseFactory $response)
    {
        parent::__construct($request, $repository, $response);
    }

    public function index()
    {
        return $this->response->view($this->getViewName('index'));
    }

    public function store()
    {
        $validatedData = $this->validateMedicine();
        $data = $this->request->all();
        //registration_image
        $registration_image = '';
        if ($this->request->hasFile('registration_image_file')) {
            $registration_image = $this->repository->saveImage($this->request->file('registration_image_file'), $this->getViewsFolder(), ['width' => 1024, 'height' => 600]);
            if (!$registration_image) {
                return redirect()->back()->withError('Could not save image');
            }
        }

        //business_registration_image
        $business_registration_image = '';
        if ($this->request->hasFile('business_registration_image_file')) {
            $business_registration_image = $this->repository->saveImage($this->request->file('business_registration_image_file'), $this->getViewsFolder(), ['width' => 1024, 'height' => 600]);
            if (!$business_registration_image) {
                return redirect()->back()->withError('Could not save image');
            }
        }

        //brand_image
        $brand_image = '';
        if ($this->request->hasFile('brand_image_file')) {
            $brand_image = $this->repository->saveImage($this->request->file('brand_image_file'), $this->getViewsFolder(), ['width' => 400, 'height' => 400]);
            if (!$brand_image) {
                return redirect()->back()->withError('Could not save image');
            }
        }

        $data['brand_image'] = $brand_image;
        $data['registration_image'] = $registration_image;
        $data['business_registration_image'] = $business_registration_image;

        $this->repository->create($data);

        return $this->redirectTo('index');
    }

    public function update($id)
    {
        //$data = $this->request->all();
        //var_dump($data);
        //die();

        $validatedData = $this->validateMedicine();
        $item = $this->repository->findOrFail($id);
        $data = $this->request->all();
        //registration_image
        $registration_image = '';
        if ($this->request->hasFile('registration_image_file')) {
            $registration_image = $this->repository->saveImage($this->request->file('registration_image_file'), $this->getViewsFolder(), ['width' => 1024, 'height' => 600]);
            if (!$registration_image) {
                return redirect()->back()->withError('Could not save image');
            }
        }else{
            $registration_image = $item->registration_image;
        }

        //business_registration_image
        $business_registration_image = '';
        if ($this->request->hasFile('business_registration_image_file')) {
            $business_registration_image = $this->repository->saveImage($this->request->file('business_registration_image_file'), $this->getViewsFolder(), ['width' => 1024, 'height' => 600]);
            if (!$business_registration_image) {
                return redirect()->back()->withError('Could not save image');
            }
        }else{
            $business_registration_image = $item->business_registration_image;
        }

        //brand_image
        $brand_image = '';
        if ($this->request->hasFile('brand_image_file')) {
            $brand_image = $this->repository->saveImage($this->request->file('brand_image_file'), $this->getViewsFolder(), ['width' => 400, 'height' => 400]);
            if (!$brand_image) {
                return redirect()->back()->withError('Could not save image');
            }
        }else{
            $brand_image = $item->brand_image;
        }

        $data['brand_image'] = $brand_image;
        $data['registration_image'] = $registration_image;
        $data['business_registration_image'] = $business_registration_image;

        $item->fill($data)->save();

        return $this->redirectTo('index');
    }

    public function validateMedicine()
    {
        if (in_array($this->request->method(), ['POST']))
            return $this->request->validate([
                'medicine_name' => 'required|max:255',
                'medicine_description' => 'required|max:255',

            ]);
        if (in_array($this->request->method(), ['PUT']))
            return $this->request->validate([
                'medicine_name' => 'required|max:255',
                'medicine_description' => 'required|max:255',
            ]);
    }
}
