@extends('layouts.app')
@section('pageTitle', 'modify medicines')
@section('content')
    <div id="header_wrapper" class="header-md ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <header id="header">
                        <h1>{{trans('Thêm mới Thuốc liên kết')}}</h1>
                        
                    </header>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="container">
        <div class="content-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <header class="card-heading ">
                            <h2 class="card-title">{{trans('Thông tin Thuốc')}}</h2>
                            <p>{{trans('Vui lòng nhập chính xác các thông tin được nêu bên dưới, các thông tin này cực kỳ quan trọng để kết nối, quản lý Thuốc.')}} </p>
                        </header>
                        <div class="card-body">
                            {!! Form::open(['route' => ['medicines.update', 'id' => $data->id], 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

                            
                            <div class="form-group{{ $errors->has('medicine_name') ? ' has-error' : '' }}">
                                {{Form::label('medicine_name', trans('Tên Thuốc'), ['class' => 'col-sm-2 control-label'])}}
                                <div class="col-sm-10">
                                    {{ Form::text('medicine_name', $data->medicine_name,
                                            [   'placeholder' => trans('Nhập thương hiệu của Thuốc'),
                                                'data-rule-required' => 'true',
                                                'minlength' => '2',
                                                'class' => 'form-control',
                                                'aria-required' => 'true',
                                                'aria-describedby' => 'medicine_name-error'
                                            ])
                                    }}
                                    @if ($errors->has('medicine_name'))
                                        <span id="medicine_name-error" class="help-block" style="display: inline;">{{ $errors->first('medicine_name') }}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('medicine_description') ? ' has-error' : '' }}">
                                {{Form::label('medicine_description', trans('Mô tả thuốc'), ['class' => 'col-sm-2 control-label'])}}
                                <div class="col-sm-10">
                                    {{ Form::text('medicine_description', $data->medicine_description,
                                            [   'placeholder' => trans('Nhập thương hiệu của Thuốc'),
                                                'data-rule-required' => 'true',
                                                'minlength' => '2',
                                                'class' => 'form-control',
                                                'aria-required' => 'true',
                                                'aria-describedby' => 'medicine_description-error'
                                            ])
                                    }}
                                    @if ($errors->has('medicine_description'))
                                        <span id="medicine_description-error" class="help-block" style="display: inline;">{{ $errors->first('medicine_description') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    {{Form::button(trans('Cập nhật'), ['type' => 'submit', 'class' => 'btn btn-primary'])}}
                                    {{link_to_route('medicines.index', 'Quay lại', null, array('class' => 'btn btn-default'))}}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section id="chat_compose_wrapper">
            <div class="tippy-top">
                <div class="recipient">Allison Grayce</div>
                <ul class="card-actions icons  right-top">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="zmdi zmdi-videocam"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu btn-primary dropdown-menu-right">
                            <li>
                                <a href="javascript:void(0)">Option One</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Option Two</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Option Three</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-chat="close">
                            <i class="zmdi zmdi-close"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class='chat-wrapper scrollbar'>
                <div class='chat-message scrollbar'>
                    <div class='chat-message chat-message-recipient'>
                        <img class='chat-image chat-image-default' src='{{URL::asset('assets/img/profiles/05.jpg') }}'/>
                        <div class='chat-message-wrapper'>
                            <div class='chat-message-content'>
                                <p>Hey Mike, we have funding for our new project!</p>
                            </div>
                            <div class='chat-details'>
                                <span class='today small'></span>
                            </div>
                        </div>
                    </div>
                    <div class='chat-message chat-message-sender'>
                        <img class='chat-image chat-image-default' src='{{URL::asset('assets/img/profiles/02.jpg') }}'/>
                        <div class='chat-message-wrapper '>
                            <div class='chat-message-content'>
                                <p>Awesome! Photo booth banh mi pitchfork kickstarter whatever, prism godard ethical
                                    90's cray selvage.</p>
                            </div>
                            <div class='chat-details'>
                                <span class='today small'></span>
                            </div>
                        </div>
                    </div>
                    <div class='chat-message chat-message-recipient'>
                        <img class='chat-image chat-image-default' src='{{URL::asset('assets/img/profiles/05.jpg') }}'/>
                        <div class='chat-message-wrapper'>
                            <div class='chat-message-content'>
                                <p> Artisan glossier vaporware meditation paleo humblebrag forage small batch.</p>
                            </div>
                            <div class='chat-details'>
                                <span class='today small'></span>
                            </div>
                        </div>
                    </div>
                    <div class='chat-message chat-message-sender'>
                        <img class='chat-image chat-image-default' src='{{URL::asset('assets/img/profiles/02.jpg') }}'/>
                        <div class='chat-message-wrapper'>
                            <div class='chat-message-content'>
                                <p>Bushwick letterpress vegan craft beer dreamcatcher kickstarter.</p>
                            </div>
                            <div class='chat-details'>
                                <span class='today small'></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer id="compose-footer">
                <form class="form-horizontal compose-form">
                    <ul class="card-actions icons left-bottom">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-attachment-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-mood"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="form-group m-10 p-l-75 is-empty">
                        <div class="input-group">
                            <label class="sr-only">Leave a comment...</label>
                            <input type="text" class="form-control form-rounded input-lightGray"
                                   placeholder="Leave a comment..">
                            <span class="input-group-btn">
                          <button type="button" class="btn btn-blue btn-fab  btn-fab-sm">
                            <i class="zmdi zmdi-mail-send"></i>
                          </button>
                        </span>
                        </div>
                    </div>
                </form>
            </footer>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
    </script>
@endpush