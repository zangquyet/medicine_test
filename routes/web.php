<?php

//CMS route
Route::namespace('CMS')->group(function () {
    Route::get('partners/getData', ['as' => 'partners.data', 'uses' => 'PartnersController@getData']);
    Route::get('', ['as' => 'homes.index', 'uses' => 'HomeController@index']);
    Route::get('partners', ['as' => 'partners.index', 'uses' => 'PartnersController@index']);
    Route::get('partners/create', ['as' => 'partners.create', 'uses' => 'PartnersController@create']);
    Route::post('partners', ['as' => 'partners.store', 'uses' => 'PartnersController@store']);
    Route::get('partners/{id}', ['as' => 'partners.show', 'uses' => 'PartnersController@show']);
    Route::get('partners/{id}/edit', ['as' => 'partners.edit', 'uses' => 'PartnersController@edit']);
    Route::put('partners/{id}', ['as' => 'partners.update', 'uses' => 'PartnersController@update']);
    Route::delete('partners/{id}', ['as' => 'partners.delete', 'uses' => 'PartnersController@destroy']);

    
    Route::get('medicines/getData', ['as' => 'medicines.data', 'uses' => 'MedicinesController@getData']);

    
    Route::get('medicines', ['as' => 'medicines.index', 'uses' => 'MedicinesController@index']);
    Route::get('medicines/create', ['as' => 'medicines.create', 'uses' => 'MedicinesController@create']);
    Route::post('medicines', ['as' => 'medicines.store', 'uses' => 'MedicinesController@store']);
    Route::get('medicines/{id}', ['as' => 'medicines.show', 'uses' => 'MedicinesController@show']);
    Route::get('medicines/{id}/edit', ['as' => 'medicines.edit', 'uses' => 'MedicinesController@edit']);
    Route::put('medicines/{id}', ['as' => 'medicines.update', 'uses' => 'MedicinesController@update']);
    Route::delete('medicines/{id}', ['as' => 'medicines.delete', 'uses' => 'MedicinesController@destroy']);
    


    //Route::resource('medicines','MedicinesController');
    Route::resource('categories','CategoryController');

});

